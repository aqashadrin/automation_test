package herokuapp;

import core.executor.SelenideExecutorUITest;
import org.testng.annotations.Test;
import pages.herokuapp.DragAndDropPage;


public class DragAndDrop_Test extends SelenideExecutorUITest {

    @Test
    public void verifyDragAndDrop_Test() {
        get(DragAndDropPage.class)
                .click_on_link()
                .verify_page_title()
                .drag_and_drop(DragAndDropPage.column.A)
                .check_drag_and_drop(DragAndDropPage.column.B.name(), DragAndDropPage.column.A.name());
        get(DragAndDropPage.class)
                .drag_and_drop(DragAndDropPage.column.B)
                .check_drag_and_drop(DragAndDropPage.column.A.name(), DragAndDropPage.column.B.name());
    }
}
