package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.DynamicContentPage;
import pages.herokuapp.HomePage;

import java.util.ArrayList;
import java.util.List;

import static pages.herokuapp.ExamplesLinksEnum.DYNAMIC_CONTENT;

public class DynamicContent_Test extends ExecutorUITest {
    List<String> imagesLinks = new ArrayList<>();
    List<String> contentTexts = new ArrayList<>();

    @Test()
    public void openDynamicContentPage_Test() {
        get(HomePage.class)
                .click_on_link(DYNAMIC_CONTENT);

        get(DynamicContentPage.class)
                .verify_dynamic_content_page_url()
                .verify_page_title();
    }


    @Test(dependsOnMethods = "openDynamicContentPage_Test", dataProvider = "count")
    public void verifyDynamicContent_Test(Integer count) {
        log.info("Attempt number :: " + count);
        imagesLinks.addAll(get(DynamicContentPage.class).get_images_links());
        contentTexts.addAll(get(DynamicContentPage.class).get_content_text());

        get(DynamicContentPage.class)
                .click_here_button();

        Assert.assertNotEquals(imagesLinks, get(DynamicContentPage.class).get_images_links());
        Assert.assertNotEquals(contentTexts, get(DynamicContentPage.class).get_content_text());
    }


    @DataProvider(name = "count")
    private Object[][] getCount() {
        return new Object[][]{
                {1},
                {2},
                {3},
                {4},
                {5},
                {6}
        };
    }
}
