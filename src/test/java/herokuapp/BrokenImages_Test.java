package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.herokuapp.BrokenImagesPage;
import pages.herokuapp.HomePage;

import static pages.herokuapp.ExamplesLinksEnum.BROKEN_IMAGES;

public class BrokenImages_Test extends ExecutorUITest {

    @Test(priority = 1)
    public void openBrokenImagesPage_Test() {
        get(HomePage.class)
                .click_on_link(BROKEN_IMAGES);

        get(BrokenImagesPage.class)
                .verify_broken_images_page_url()
                .verify_page_title();

    }

    @Test(dependsOnMethods = "openBrokenImagesPage_Test")
    public void verifyBrokenImages_Test() {
        log.info("Verify Broken Images" + getClass().getSimpleName());
        Assert.assertTrue(get(BrokenImagesPage.class).get_images_natural_width(1) == 0);
        Assert.assertTrue(get(BrokenImagesPage.class).get_images_natural_width(2) == 0);

        log.info("Verify Not Broken Images");
        Assert.assertTrue(get(BrokenImagesPage.class).get_images_natural_width(3) == 160);
        Assert.assertTrue(get(BrokenImagesPage.class).get_git_hub_images_natural_width() == 149);
    }
}
