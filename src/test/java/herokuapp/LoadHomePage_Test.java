package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.annotations.Test;
import pages.herokuapp.HomePage;

public class LoadHomePage_Test extends ExecutorUITest {

    @Test(description = "Load Home page")
    public void verifyPageHeader_Test() {
        get(HomePage.class)
                .load_page()
                .verify_home_page_url();

        get(HomePage.class)
                .verify_title_txt("Welcome to the-internet")
                .verify_sub_title_txt("Available Examples");
    }
}
