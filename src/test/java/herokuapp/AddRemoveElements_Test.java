package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.AddRemoveElementsPage;
import pages.herokuapp.HomePage;

import static pages.herokuapp.ExamplesLinksEnum.ADD_REMOVE;

public class AddRemoveElements_Test extends ExecutorUITest {

    @Test
    public void openAddRemoveElementsPage_Test() {
        get(HomePage.class)
                .click_on_link(ADD_REMOVE);

        get(AddRemoveElementsPage.class)
                .verify_add_remove_page_url();
    }


    @Test(dependsOnMethods = "openAddRemoveElementsPage_Test")
    public void verifyPageHeader_Test() {
        get(AddRemoveElementsPage.class)
                .verify_page_title();
    }


    @Test(dependsOnMethods = "verifyPageHeader_Test", dataProvider = "count")
    public void verifyDeleteButtonsCount_Test(int count) {
        get(AddRemoveElementsPage.class)
                .click_add_element_button(count);

        get(AddRemoveElementsPage.class)
                .verify_delete_btn_count(count);

        get(AddRemoveElementsPage.class)
                .click_all_delete_buttons();
    }


    @Test(dependsOnMethods = "verifyDeleteButtonsCount_Test", alwaysRun = true)
    public void verifyPageFooter_Test() {
        get(HomePage.class)
                .verify_page_footer_txt("Powered by Elemental Selenium");
    }


    @DataProvider(name = "count")
    private Object[][] getCount() {
        return new Object[][]{
                {1},
                {2},
                {3},
                {4},
                {5},
                {6},
                {7},
                {8},
                {9},
                {10}
        };
    }
}
