package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.annotations.Test;
import pages.herokuapp.ABTestVariationPage;
import pages.herokuapp.HomePage;

import static pages.herokuapp.ExamplesLinksEnum.AB_TESTING;

public class ABTestVariation_Test extends ExecutorUITest {

    @Test(priority = 1)
    public void openABTestVariationPage_Test() {
        get(HomePage.class)
                .click_on_link(AB_TESTING);

        get(ABTestVariationPage.class)
                .verify_abt_page_url();
    }


    @Test(priority = 2)
    public void verifyPageHeader_Test() {
        get(ABTestVariationPage.class)
                .verify_page_title();
    }


    @Test(priority = 3)
    public void verifyPageBody_Test() {
        get(ABTestVariationPage.class)
                .verify_page_body("Also known as split testing. This is a way in which businesses are able to simultaneously test and learn different versions of a page to see which text and/or functionality works best towards a desired outcome (e.g. a user action such as a click-through).");
    }


    @Test(priority = 4)
    public void verifyPageFooter_Test() {
        get(HomePage.class)
                .verify_page_footer_txt("Powered by Elemental Selenium");
    }
}
