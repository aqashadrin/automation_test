package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.BasicAuthPage;
import pages.herokuapp.HomePage;

import static pages.herokuapp.ExamplesLinksEnum.BASIC_AUTH;

public class BasicAuth_Test extends ExecutorUITest {

    @Test
    public void navigateToBasicAuth_Test() {
        get(HomePage.class)
                .click_on_link(BASIC_AUTH);

        get(BasicAuthPage.class)
                .verify_basic_auth_page_url();
    }


    @Test(dependsOnMethods = "navigateToBasicAuth_Test", dataProvider = "auth")
    public void enterLoginAndPassword_Test(String login, String password, Boolean auth) {
        get(BasicAuthPage.class)
                .enter_login_and_password(login, password);

        if (auth) {
            get(BasicAuthPage.class)
                    .verify_title_txt()
                    .verify_body_txt();
        } else {
            get(BasicAuthPage.class)
                    .verify_bad_auth();
        }
    }


    @DataProvider(name = "auth")
    private Object[][] getAuth() {
        return new Object[][]
                {
                        {"test", "test", false},
                        {"admin", "Test", false},
                        {"test", "admin", false},
                        {"admin", "admin", true}
                };
    }
}
