package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.CheckboxesPage;
import pages.herokuapp.HomePage;

import static pages.herokuapp.ExamplesLinksEnum.CHECKBOXES;

public class Checkboxes_Test extends ExecutorUITest {

    @Test(priority = 1)
    public void openCheckboxesPage_Test() {
        get(HomePage.class)
                .click_on_link(CHECKBOXES);

        get(CheckboxesPage.class)
                .verify_checkboxes_page_url()
                .verify_checkboxes_page_title();
    }


    @Test(dependsOnMethods = "openCheckboxesPage_Test", dataProvider = "checkbox")
    public void verifyCheckBoxIsChecked_Test(Integer num) {
        get(CheckboxesPage.class)
                .checked_checkbox_btn(num);

        get(CheckboxesPage.class)
                .verify_checkbox_is_checked(num, true);
    }


    @Test(dependsOnMethods = "verifyCheckBoxIsChecked_Test", dataProvider = "checkbox")
    public void verifyCheckBoxIsUnChecked_Test(Integer num) {
        get(CheckboxesPage.class)
                .unchecked_checkbox_btn(num);

        get(CheckboxesPage.class)
                .verify_checkbox_is_checked(num, false);
    }


    @DataProvider(name = "checkbox")
    private Object[][] getCheckBoxNum() {
        return new Object[][]{
                {1},
                {2}
        };
    }
}
