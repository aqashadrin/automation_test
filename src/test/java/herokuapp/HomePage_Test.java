package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.ExamplesLinksEnum;
import pages.herokuapp.HomePage;

import java.util.Arrays;
import java.util.List;

public class HomePage_Test extends ExecutorUITest {

    @BeforeClass
    public void precondition() {
        get(HomePage.class)
                .load_page()
                .verify_home_page_url();
    }


    @Test(priority = 1)
    public void verifyPageHeader_Test() {
        get(HomePage.class)
                .verify_title_txt("Welcome to the-internet")
                .verify_sub_title_txt("Available Examples");
    }


    @Test(priority = 2)
    public void verifyPageBody_Test() {
        List<String> actualListData = get(HomePage.class).get_page_elements_texts();
        //verify size lists size between actual and expected
        Assert.assertEquals(actualListData.size(), Arrays.asList(ExamplesLinksEnum.values()).size());
        //verify that expected data contains in actual data
        for (ExamplesLinksEnum link : ExamplesLinksEnum.values()) {
            Assert.assertTrue(actualListData.contains(link.getName()), "Not found :: " + link.getName() + " in\n" + actualListData);
        }
    }


    @Test(priority = 3)
    public void verifyPageFooter_Test() {
        get(HomePage.class)
                .verify_page_footer_txt("Powered by Elemental Selenium");
    }
}
