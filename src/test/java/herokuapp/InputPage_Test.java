package herokuapp;

import core.executor.SelenideExecutorUITest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.InputsPage;

public class InputPage_Test extends SelenideExecutorUITest {

    @BeforeTest
    public void preconditions() {
        get(InputsPage.class)
                .click_on_link()
                .verify_input_page();
    }

    @Test(dataProvider = "data")
    public void verifyInputPageValues_Test(String actualData, String expectedData) {
        get(InputsPage.class)
                .enter_input_page(actualData)
                .verify_input_value(expectedData);
    }

    @DataProvider(name = "data")
    private Object[][] getData() {
        return new Object[][]{
                {"12345", "12345"},
                {"-12345", "-12345"},
                {"0.12345", "0.12345"},
                {"-0.12345", "-0.12345"},
                {"1234a", "1234"},
                {"123a!", "123"},
                {"!@#$%^&*()_", ""},
                {"ZXCVBNM", ""},
                {"!A", ""}
        };
    }
}
