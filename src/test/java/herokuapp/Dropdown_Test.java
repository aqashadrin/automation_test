package herokuapp;

import core.executor.ExecutorUITest;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.herokuapp.DropdownPage;
import pages.herokuapp.HomePage;

import java.util.Arrays;
import java.util.List;

import static pages.herokuapp.ExamplesLinksEnum.DROPDOWN;

public class Dropdown_Test extends ExecutorUITest {
    private List<String> actualOptions = Arrays.asList("Please select an option", "Option 1", "Option 2");

    @Test
    public void openBrokenImagesPage_Test() {
        get(HomePage.class)
                .click_on_link(DROPDOWN);

        get(DropdownPage.class)
                .verify_dropdown_page_url()
                .verify_page_title();

        get(DropdownPage.class)
                .verify_option_is_selected(actualOptions.get(0));
    }


    @Test(dependsOnMethods = "openBrokenImagesPage_Test", dataProvider = "options")
    public void verifyDropDown_Test(Integer option) {
        Assert.assertEquals(actualOptions, get(DropdownPage.class).get_options_txt());

        get(DropdownPage.class)
                .select_dropdown_by_text(actualOptions.get(option))
                .verify_option_is_selected(actualOptions.get(option));

        get(DropdownPage.class)
                .select_dropdown_by_value(option.toString())
                .verify_option_is_selected(actualOptions.get(option));
    }


    @DataProvider(name = "options")
    private Object[][] getOptions() {
        return new Object[][]{
                {1},
                {2},
        };
    }
}
