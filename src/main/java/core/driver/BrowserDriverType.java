package core.driver;

public enum BrowserDriverType {
    CHROME, FIREFOX, IE
}
