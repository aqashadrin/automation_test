package core.driver;

import core.file_reader.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverConstructor {
    private static ThreadLocal<WebDriver> treadDriver = new ThreadLocal<>();

    public DriverConstructor(BrowserDriverType browserDriverType, PropertiesReader propertiesReader) {
        if (treadDriver.get() == null) {
            createDriverConstructor(browserDriverType, propertiesReader);
        }
    }

    private void createDriverConstructor(BrowserDriverType browserDriverType, PropertiesReader propertiesReader) {
        switch (browserDriverType) {
            case CHROME:
                ChromeOptions options = new ChromeOptions();
                options.addArguments(propertiesReader.getProperties().getProperty("chrome.browser.arguments").split(":"));
                this.treadDriver.set(new ChromeDriver(options));
                break;
            case FIREFOX:
                this.treadDriver.set(new FirefoxDriver());
                break;
            case IE:
                this.treadDriver.set(new EdgeDriver());
                break;
        }
    }

    public WebDriver getDriver() {
        return this.treadDriver.get();
    }

    public void quitDriver() {
        if (this.treadDriver.get() != null) {
            this.treadDriver.get().close();
            this.treadDriver.get().quit();
            this.treadDriver.remove();
        }
    }
}
