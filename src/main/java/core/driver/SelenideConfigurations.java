package core.driver;

import com.codeborne.selenide.Configuration;
import core.file_reader.PropertiesReader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SelenideConfigurations {

    public SelenideConfigurations(PropertiesReader propertiesReader) {
        setSelenideConfiguration(propertiesReader);
    }

    private void setSelenideConfiguration(PropertiesReader propertiesReader) {
        Map<String, String> properties = new HashMap<>();
        Arrays.asList(propertiesReader.getProperties().getProperty("selenide.configs").split(":")).forEach(property -> {
            if (!property.isEmpty()) properties.put(property.split("=")[0], property.split("=")[1]);
        });
        properties.forEach((property, value) -> {
            Configuration.browserSize = property.equals("browserSize") ? value : Configuration.browserSize;
            Configuration.headless = property.equals("headless") ? Boolean.parseBoolean(value) : Configuration.headless;
        });
    }
}
