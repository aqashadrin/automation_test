package core.element_controller;

import com.paulhammant.ngwebdriver.NgWebDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public abstract class WebElementsController<T> {
    protected Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    protected WebDriver driver;
    protected Properties properties;

    protected T click(By locator) {
        this.driver.findElement(locator).click();
        return me();
    }

    protected T enter(By locator, String text) {
        this.driver.findElement(locator).sendKeys(new CharSequence[]{text});
        return me();
    }

    protected Integer find_elements_count(By locator) {
        return this.driver.findElements(locator).size();
    }

    protected T wait_until(Integer timeout) {
        try {
            Thread.sleep(timeout * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return me();
    }

    protected T wait_until(ExpectedCondition<?> condition) {
        wait_until(condition, 60, 1000);
        return me();
    }

    protected T wait_until(ExpectedCondition<?> condition, Integer timeout, Integer sleep) {
        new WebDriverWait(this.driver, timeout, sleep).ignoring(NotFoundException.class, ElementNotVisibleException.class).until(condition);
        return me();
    }

    protected T verify_visibility_of_element(By element) {
        return wait_until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    protected T verify_element_by_txt(By element, String text) {
        return wait_until(ExpectedConditions.textToBe(element, text));
    }

    protected T verify_element_clickable(By locator) {
        return wait_until(ExpectedConditions.elementToBeClickable(locator));
    }

    public Boolean is_element_exist(By locator) {
        List<WebElement> elements = this.driver.findElements(locator);
        return elements.size() > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public T wait_until_page_loaded() {
        Integer timeout = 0;
        while (timeout++ <= 60) {
            if (((JavascriptExecutor) this.driver).executeScript("return document.readyState").toString().equals("complete")) {
                return me();
            } else {
                wait_until(1);
            }
        }
        Assert.fail("The page is not loaded completely within the timeout");
        return me();
    }

    protected T wait_util_angular_elements_is_loaded() {
        new NgWebDriver((JavascriptExecutor) this.driver).waitForAngularRequestsToFinish();
        return me();
    }

    protected T load(String configUrl) {
        Assert.assertNotNull(configUrl);
        this.driver.get(properties.getProperty(configUrl));
        return me();
    }

    protected T loadUrl(String url) {
        Assert.assertNotNull(url);
        this.driver.get(url);
        return me();
    }

    protected String get_page_url() {
        return this.driver.getCurrentUrl();
    }

    protected T verify_page_url(String pageUrl) {
        Assert.assertEquals(get_page_url(), pageUrl);
        return me();
    }

    protected T verify_config_page_url(String partUrl) {
        Assert.assertEquals(get_page_url(), properties.getProperty("url") + partUrl);
        return me();
    }

    protected String find_element_attribute(By locator, String attribute) {
        WebElement element = this.driver.findElement(locator);
        return element.getAttribute(attribute);
    }

    protected List<String> find_elements_attribute(By locator, String attribute) {
        List<String> attributes = new ArrayList<>();
        for (WebElement element : this.driver.findElements(locator)) {
            attributes.add(element.getAttribute(attribute));
        }
        return attributes;
    }

    protected String find_element_text(By locator) {
        WebElement element = this.driver.findElement(locator);
        return element.getText();
    }

    protected List<String> find_elements_text(By locator) {
        List<String> elementsText = new ArrayList<>();
        for (WebElement element : this.driver.findElements(locator)) {
            elementsText.add(element.getText());
        }
        return elementsText;
    }

    protected T send_key(By locator, CharSequence... keys) {
        this.driver.findElement(locator).sendKeys(keys);
        return me();
    }

    protected T send_key(CharSequence... keys) {
        Actions actions = new Actions(this.driver);
        actions.sendKeys(keys).build().perform();
        return me();
    }

    protected T select_by_value(By locator, String value) {
        Select select = new Select(this.driver.findElement(locator));
        select.selectByValue(value);
        return me();
    }

    protected T select_by_text(By locator, String text) {
        Select select = new Select(this.driver.findElement(locator));
        select.selectByVisibleText(text);
        return me();
    }

    protected String find_selected_option(By locator) {
        Select select = new Select(this.driver.findElement(locator));
        return select.getFirstSelectedOption().getText();
    }

    protected T me() {
        return (T) this;
    }
}
