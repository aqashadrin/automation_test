package core.element_controller;

import com.codeborne.selenide.SelenideElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.page;

public abstract class SelenideWebElementController<T> {
    protected Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    protected T click(SelenideElement element) {
        element.click();
        return me();
    }

    protected T me() {
        return page((T) this);
    }
}
