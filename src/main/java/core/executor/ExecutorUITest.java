package core.executor;

import core.driver.BrowserDriverType;
import core.driver.DriverConstructor;
import core.element_controller.WebElementsController;
import core.file_reader.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.xml.XmlClass;

import java.lang.reflect.Field;
import java.util.List;

public abstract class ExecutorUITest {
    private String browserType;
    private PropertiesReader propertiesReader;
    private DriverConstructor driverConstructor;
    private ITestContext context;
    protected Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    @BeforeClass(alwaysRun = true)
    protected void startTest(ITestContext context) {
        this.context = context;
        this.browserType = this.context.getSuite().getParameter("browser");
        this.propertiesReader = new PropertiesReader(this.context);
        this.driverConstructor = new DriverConstructor(BrowserDriverType.valueOf(browserType.toUpperCase()), propertiesReader);
    }

    protected <T> T get(Class<T> pageClass, WebDriver webDriver) {
        try {
            T page = pageClass.newInstance();
            Field driver = WebElementsController.class.getDeclaredField("driver");
            driver.setAccessible(true);
            driver.set(page, webDriver);
            Field properties = WebElementsController.class.getDeclaredField("properties");
            properties.setAccessible(true);
            properties.set(page, this.propertiesReader.getProperties());
            return page;
        } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected <T> T get(Class<T> pageClass) {
        return get(pageClass, this.driverConstructor.getDriver());
    }

    @AfterClass
    public void closeDriver(ITestContext context) {
        List<XmlClass> xmlClasses = context.getCurrentXmlTest().getClasses();
        if (xmlClasses.get(xmlClasses.size() - 1).getSupportClass().getSimpleName().equals(this.getClass().getSimpleName())
                || context.getFailedConfigurations().size() != 0) {
            driverConstructor.quitDriver();
        }
    }
}
