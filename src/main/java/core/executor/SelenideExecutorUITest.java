package core.executor;

import core.driver.SelenideConfigurations;
import core.file_reader.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.driver;

public abstract class SelenideExecutorUITest {
    private PropertiesReader propertiesReader;
    private ITestContext context;
    private SelenideConfigurations selenideConfigurations;
    protected Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    @BeforeTest(alwaysRun = true)
    protected void startTest(ITestContext context) {
        this.context = context;
        this.propertiesReader = new PropertiesReader(this.context);
        this.selenideConfigurations = new SelenideConfigurations(this.propertiesReader);
    }

    protected <T> T get(Class<T> pageClass) {
        return driver().hasWebDriverStarted() ? page(pageClass) : open(propertiesReader.getProperties().getProperty("url"), pageClass);
    }

    protected <T> T get(Class<T> pageClass, String url) {
        return driver().hasWebDriverStarted() ? page(pageClass) : open(url, pageClass);
    }
}
