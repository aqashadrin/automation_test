package pages.google;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;

public class GoogleMainPage extends WebElementsController<GoogleMainPage> {
    private By searchDataTxt = By.name("q");

    private By get_find_records_xpath(String elementNumber) {
        return By.xpath("(//div[@class='rc']//div[@class='r']//h3)[" + elementNumber + "]");
    }

    private By get_find_records_descriptions_xpath(String elementNumber) {
        return By.xpath("(//div[@class='rc']//div[@class='s']//span)[" + elementNumber + "]");
    }

    public GoogleMainPage load_page() {
        return load("url");
    }

    public GoogleMainPage enter_search_data(String searchData) {
        wait_util_angular_elements_is_loaded();
        enter(searchDataTxt, searchData);
        send_key(searchDataTxt, Keys.ENTER);
        return me();
    }

    public List<String> get_find_records_data_txt() {
        wait_util_angular_elements_is_loaded();
        return find_elements_text(get_find_records_xpath(".."));
    }

    public List<String> get_find_records_descriptions_data_txt() {
        wait_util_angular_elements_is_loaded();
        return find_elements_text(get_find_records_descriptions_xpath(".."));
    }

    public Integer get_find_records_count() {
        wait_util_angular_elements_is_loaded();
        return find_elements_count(get_find_records_xpath(".."));
    }
}
