package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;

import java.util.List;

public class DynamicContentPage extends WebElementsController<DynamicContentPage> {

    private By titleTxt = By.tagName("h3");
    private By imagesLinks = By.xpath("//div[@class ='large-2 columns']//img");
    private By contentTexts = By.xpath("//div[@class ='large-10 columns']");
    private By clickHereBtn = By.xpath("//*[text()='click here']");

    public DynamicContentPage verify_dynamic_content_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("dynamic_content");
    }

    public DynamicContentPage verify_page_title() {
        return verify_element_by_txt(titleTxt, "Dynamic Content");
    }

    public List<String> get_images_links() {
        return find_elements_attribute(imagesLinks, "src");
    }

    public List<String> get_content_text() {
        return find_elements_text(contentTexts);
    }

    public DynamicContentPage click_here_button() {
        return click(clickHereBtn);
    }

}
