package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;

public class DropdownPage extends WebElementsController<DropdownPage> {

    private By titleTxt = By.tagName("h3");
    private By dropdown = By.id("dropdown");
    private By optionsTxt = By.xpath("//select//option");

    public DropdownPage verify_dropdown_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("dropdown");
    }

    public DropdownPage verify_page_title() {
        return verify_element_by_txt(titleTxt, "Dropdown List");
    }

    public DropdownPage select_dropdown_by_text(String text) {
        select_by_text(dropdown, text);
        return me();
    }

    public DropdownPage select_dropdown_by_value(String value) {
        select_by_value(dropdown, value);
        return me();
    }

    public List<String> get_options_txt() {
        return find_elements_text(optionsTxt);
    }

    public DropdownPage verify_option_is_selected(String option) {
        Assert.assertEquals(find_selected_option(dropdown), option);
        return me();
    }
}
