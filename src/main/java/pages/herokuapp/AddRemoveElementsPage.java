package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.testng.Assert;

public class AddRemoveElementsPage extends WebElementsController<AddRemoveElementsPage> {
    private By pageTitle = By.tagName("h3");
    private By addElementBtn = By.xpath("//button[text()='Add Element']");

    private By delete_element_btn_xpath(String elementNum) {
        return By.xpath("(//button[text()='Delete'])[" + elementNum + "]");
    }

    public AddRemoveElementsPage verify_add_remove_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("add_remove_elements/");
    }

    public AddRemoveElementsPage verify_page_title() {
        return verify_element_by_txt(this.pageTitle, "Add/Remove Elements");
    }

    public AddRemoveElementsPage click_add_element_button() {
        return click(addElementBtn);
    }

    public AddRemoveElementsPage click_add_element_button(Integer count) {
        while (count-- != 0) {
            click_add_element_button();
        }
        return me();
    }

    public int get_delete_btn_count() {
        return find_elements_count(delete_element_btn_xpath(".."));
    }

    public AddRemoveElementsPage verify_delete_btn_count(int expectedResult) {
        int actualResult = get_delete_btn_count();
        Assert.assertEquals(actualResult, expectedResult);
        return me();
    }

    public AddRemoveElementsPage click_delete_button(Integer buttonNumber) {
        return click(delete_element_btn_xpath(Integer.toString(buttonNumber)));
    }

    public AddRemoveElementsPage click_delete_button() {
        return click_delete_button(1);
    }

    public AddRemoveElementsPage click_all_delete_buttons() {
        int buttonCount = get_delete_btn_count();
        while (0 < buttonCount--) {
            click_delete_button();
            Assert.assertEquals(buttonCount, get_delete_btn_count());
        }
        return me();
    }
}
