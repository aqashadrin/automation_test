package pages.herokuapp;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.element_controller.SelenideWebElementController;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DragAndDropPage extends SelenideWebElementController<DragAndDropPage> {

    @FindBy(how = How.XPATH, using = "//a[text()='Drag and Drop']")
    SelenideElement link;

    @FindBy(how = How.CSS, using = "h3")
    SelenideElement title;

    @FindBy(how = How.CSS, using = "#column-a")
    SelenideElement columnA;

    @FindBy(how = How.CSS, using = "#column-b")
    SelenideElement columnB;

    @FindBy(how = How.CSS, using = "div.column header")
    ElementsCollection columns;

    public enum column {
        A, B
    }

    public DragAndDropPage click_on_link() {
        click(this.link);
        return me();
    }

    public DragAndDropPage verify_page_title() {
        this.title.shouldHave(Condition.text("Drag and Drop"));
        return me();
    }

    public DragAndDropPage drag_and_drop(column moveToElement) {
        if (moveToElement.equals(column.A)) {
            this.columnB.dragAndDropTo(this.columnA);
        } else {
            this.columnA.dragAndDropTo(this.columnB);
        }
        return me();
    }

    public DragAndDropPage check_drag_and_drop(String ...values){
        columns.shouldHave(CollectionCondition.exactTexts(values));
        return me();
    }
}
