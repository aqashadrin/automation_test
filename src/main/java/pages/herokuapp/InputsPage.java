package pages.herokuapp;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import core.element_controller.SelenideWebElementController;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class InputsPage extends SelenideWebElementController<InputsPage> {
    @FindBy(how = How.XPATH, using = "//li/a[text()='Inputs']")
    SelenideElement link;

    @FindBy(how = How.CSS, using = "input[type ='number']")
    SelenideElement inputField;

    @FindBy(how = How.TAG_NAME, using = "h3")
    SelenideElement pageTitle;

    @FindBy(how = How.TAG_NAME, using = "p")
    SelenideElement filedTitle;

    public InputsPage click_on_link() {
        this.link.click();
        return me();
    }

    public InputsPage verify_input_page(){
        this.pageTitle.shouldHave(Condition.text("Inputs"));
        this.filedTitle.shouldHave(Condition.text("Number"));
        this.inputField.should(Condition.exist);
        return me();
    }

    public InputsPage enter_input_page(String text) {
        this.inputField.clear();
        this.inputField.setValue(text);
        return me();
    }

    public InputsPage verify_input_value(String text){
        this.inputField.shouldHave(Condition.value(text));
        return me();
    }
}
