package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;

import java.util.List;


public class HomePage extends WebElementsController<HomePage> {
    private By titleTxt = By.tagName("h1");
    private By subTitleTxt = By.tagName("h2");
    private By listElementsTxt = By.xpath("//li//a");
    private By pageFooter = By.id("page-footer");

    private By get_link_xpath(ExamplesLinksEnum link) {
        return By.xpath("//li//a[text()='" + link.getName() + "']");
    }

    public HomePage verify_home_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("");
    }

    public HomePage load_page() {
        return load("url");
    }

    public HomePage verify_title_txt(String titleTxt) {
        return verify_element_by_txt(this.titleTxt, titleTxt);
    }

    public HomePage verify_sub_title_txt(String subTitleTxt) {
        return verify_element_by_txt(this.subTitleTxt, subTitleTxt);
    }

    public List<String> get_page_elements_texts() {
        return find_elements_text(listElementsTxt);
    }

    public HomePage click_on_link(ExamplesLinksEnum link) {
        log.info("Click on link.");
        return click(get_link_xpath(link));
    }

    public HomePage verify_page_footer_txt(String pageFooter) {
        return verify_element_by_txt(this.pageFooter, pageFooter);
    }
}
