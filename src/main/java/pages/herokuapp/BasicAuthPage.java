package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.testng.Assert;

import static org.apache.commons.lang3.StringUtils.substringAfter;

public class BasicAuthPage extends WebElementsController<BasicAuthPage> {
    private By title = By.tagName("h3");
    private By body = By.tagName("p");

    public BasicAuthPage verify_basic_auth_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("basic_auth");
    }

    public BasicAuthPage verify_title_txt() {
        verify_element_by_txt(title, "Basic Auth");
        return me();
    }

    public BasicAuthPage verify_body_txt() {
        verify_element_by_txt(body, "Congratulations! You must have the proper credentials.");
        return me();
    }

    public BasicAuthPage verify_bad_auth() {
        Assert.assertFalse(is_element_exist(title));
        Assert.assertFalse(is_element_exist(body));
        return me();
    }

    public BasicAuthPage enter_login_and_password(String login, String password) {
        loadUrl("https://" + login + ":" + password + "@" + substringAfter(properties.getProperty("url"), "://") + "basic_auth");
        return me();
    }
}
