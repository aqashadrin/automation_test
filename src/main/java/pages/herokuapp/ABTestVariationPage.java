package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.testng.Assert;

public class ABTestVariationPage extends WebElementsController<ABTestVariationPage> {
    private By pageTitle = By.tagName("h3");
    private By pageBody = By.xpath("//p[normalize-space()]");

    public ABTestVariationPage verify_abt_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("abtest");
    }

    public ABTestVariationPage verify_page_title() {
        String pageTitle = find_element_text(this.pageTitle);
        log.info("Page title ::" + pageTitle);
        Assert.assertTrue(pageTitle.contains("A/B Test Control") || pageTitle.contains("A/B Test Variation 1"), "Page title is :: " + pageTitle);
        return me();
    }

    public ABTestVariationPage verify_page_body(String pageBody) {
        return verify_element_by_txt(this.pageBody, pageBody);
    }
}
