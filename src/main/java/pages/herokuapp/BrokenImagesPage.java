package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;

public class BrokenImagesPage extends WebElementsController<BrokenImagesPage> {
    private By pageTitleTxt = By.tagName("h3");
    private By gitHubImgXpath = By.xpath("//a//img");

    private By get_images_xpath(Integer number) {
        return By.xpath("(//h3//parent::div//img)[" + number + "]");
    }

    public BrokenImagesPage verify_broken_images_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("broken_images");
    }

    public BrokenImagesPage verify_page_title() {
        return verify_element_by_txt(pageTitleTxt, "Broken Images");
    }

    public Integer get_images_natural_width(Integer imgNumber) {
        return Integer.parseInt(find_element_attribute(get_images_xpath(imgNumber), "naturalWidth"));
    }

    public Integer get_git_hub_images_natural_width() {
        return Integer.parseInt(find_element_attribute(gitHubImgXpath, "naturalWidth"));
    }
}
