package pages.herokuapp;

import core.element_controller.WebElementsController;
import org.openqa.selenium.By;
import org.testng.Assert;

public class CheckboxesPage extends WebElementsController<CheckboxesPage> {

    private By pageTitleTxt = By.tagName("h3");

    private By get_checkboxes_xpath(Integer num) {
        return By.xpath("(//input)[" + num + "]");
    }

    public CheckboxesPage verify_checkboxes_page_url() {
        wait_until_page_loaded();
        return verify_config_page_url("checkboxes");
    }

    public CheckboxesPage verify_checkboxes_page_title() {
        return verify_element_by_txt(pageTitleTxt, "Checkboxes");
    }

    public CheckboxesPage click_checkbox_btn(Integer num) {
        return click(get_checkboxes_xpath(num));
    }

    public Boolean get_checkbox_status(Integer num) {
        if (find_element_attribute(get_checkboxes_xpath(num), "checked") == null) {
            return false;
        } else if (find_element_attribute(get_checkboxes_xpath(num), "checked").equals("true")) {
            return true;
        }
        return false;
    }

    public CheckboxesPage verify_checkbox_is_checked(Integer num, Boolean status) {
        Assert.assertEquals(get_checkbox_status(num), status);
        return me();
    }

    public CheckboxesPage checked_checkbox_btn(Integer num) {
        if (!get_checkbox_status(num)) {
            return click_checkbox_btn(num);
        }
        return me();
    }

    public CheckboxesPage unchecked_checkbox_btn(Integer num) {
        if (get_checkbox_status(num)) {
            return click_checkbox_btn(num);
        }
        return me();
    }
}
